/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.gui.listeners;

import com.alexander.elguezabal1.Library;
import com.alexander.elguezabal1.gui.LibraryPanel;
import com.alexander.elguezabal1.gui.RadioButtonPanel;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author Alex
 */
public class SearchBarListener implements KeyListener, MouseListener, ActionListener {

    private RadioButtonPanel radioButtonPanel;
    
    public SearchBarListener(RadioButtonPanel radioButtonPanel) {
        this.radioButtonPanel = radioButtonPanel;
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent evt) {
        if(evt.getSource() instanceof JTextField) {
            JTextField jTextField = (JTextField) evt.getSource();
            
            // Always change the color
            jTextField.setForeground(Color.BLACK);
            if(jTextField.getText().equals(radioButtonPanel.getTEXT_AREA_PREFILL())) { jTextField.setText("");}
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if(evt.getSource() instanceof JButton) { 
            Library.frame.getLibraryTable().changeText(Library.libraryManager.findSearchedBooks(radioButtonPanel.getSearchBox().getText()));
        }
    }
    
}
