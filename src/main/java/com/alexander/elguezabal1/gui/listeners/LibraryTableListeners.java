/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.gui.listeners;

import com.alexander.elguezabal1.gui.LibraryPanel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JList;

/**
 *
 * @author Alex
 */
public class LibraryTableListeners implements MouseListener {

    private LibraryPanel libraryTable;
    
    public LibraryTableListeners(LibraryPanel libraryTable) {
        this.libraryTable = libraryTable;
    }
    
    @Override
    public void mouseClicked(MouseEvent evt) {
        
        if(!(evt.getSource() instanceof JList))
            return;
        
       
         
        JList list = (JList) evt.getSource();
        int index = list.locationToIndex(evt.getPoint());
        System.out.println(list.getSelectedValue());
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
}
