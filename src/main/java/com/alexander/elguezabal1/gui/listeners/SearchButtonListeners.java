/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.gui.listeners;

import com.alexander.elguezabal1.Library;
import com.alexander.elguezabal1.gui.RadioButtonPanel;
import com.alexander.elguezabal1.library.SearchType;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JRadioButton;

/**
 *
 * @author Alex
 */
public class SearchButtonListeners implements ItemListener{
    
    private RadioButtonPanel radioButtonPanel;
    
    public SearchButtonListeners(RadioButtonPanel radioButtonPanel) {
        this.radioButtonPanel = radioButtonPanel;
    }

    @Override
    public void itemStateChanged(ItemEvent evt) {
        if(evt.getSource() instanceof JRadioButton) {
            JRadioButton jRadioButton = (JRadioButton) evt.getSource();
            
            if(jRadioButton.isSelected()) {
                // If the Button is from the header
                System.out.println(jRadioButton.getText());
                
                // Happens when no appropriet head option is selected
                if(SearchType.getFromButtons() == null) return;
                
                // Updating GUI Here
                Library.frame.getLibraryTable().changeText(Library.libraryManager.findFoundBooks(SearchType.getFromButtons(), jRadioButton.getText()));
            }
        }    
    }
    
}
