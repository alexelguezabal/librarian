/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.gui;

import com.alexander.elguezabal1.Library;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

/**
 *
 * @author Alex
 */
public class LibraryPanel extends JPanel {
    
    private Frame frame;
    
    private JTextArea jTextArea;
    private JScrollPane scrollPane;
    
    private JButton exitButton;

    /**
     * Constructor for Library table
     * 
     * @param frame instance frame for this panel
     */
    public LibraryPanel(Frame frame) {
        // Sets the instance of the frame
        this.frame = frame;
        
        //this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.setLayout(new BorderLayout(10,10));
        this.setSize(1000,500);

        //((BorderLayout) this.getLayout()).setVgap(10);
        
        init();
        
        setMaximumSize(getPreferredSize());
    }
    
    /*
    Initiziles this JList table
    */
    private void init() { 
        // Label for the header of the page
        JLabel informationLabel = new JLabel("                             Title |  Page Count | Publisher | Author/Language | Version Number/Genre/Topic | Part of Series/Illustator ");
        informationLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        informationLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));
        
        // Working code
        this.jTextArea = new JTextArea();
        //jTextArea.setSize(new Dimension(1000,300));
        this.jTextArea.setWrapStyleWord(true);
        this.jTextArea.setLineWrap(true);
        this.jTextArea.setEditable(false);
        this.jTextArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 10));
        
        String output = "";
        for (String n : Library.libraryManager.getBooksInformation()) {
                output += n + "\n";
        }
        this.jTextArea.setText(output);       
        
        //((DefaultListCellRenderer) this.list.getCellRenderer()).setHorizontalAlignment(SwingConstants.RIGHT);
        
        // Scroll Bar (https://www.tutorialspoint.com/how-to-add-scrollbar-to-jlist-in-java)
        this.scrollPane = new JScrollPane();
        this.scrollPane.setViewportView(jTextArea);
        this.scrollPane.setBounds(0, 0, 1000, 200);
        
        // Closes the GUI.
        this.exitButton = new JButton("Exit");
        this.exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
                
        add(informationLabel, BorderLayout.WEST);        
        add(scrollPane, BorderLayout.AFTER_LAST_LINE);
        add(this.exitButton, BorderLayout.AFTER_LINE_ENDS);
    }
    
    /**
     * Changes the text in the JTextArea
     * @param text the list of String the panel should be changed to.
     */
    public void changeText(List<String> text) {
        String output = "";

        // Determines if there is anything in text, if so it is added, if not an error message is added.
        if (text.isEmpty()) {
            output = "  No Books Found  ";
        } else {
            for (String n : text) {
                output += n + "\n";
            }
        }
        this.jTextArea.setText(output);

        // Updates the pane
        validate();
        frame.validate();
        frame.repaint();
    }   
    
    /**
     * Displays all the books in the library table
     */
    public void displayAll() {
        changeText(Library.libraryManager.getBooksInformation());
    }
    
    
}
