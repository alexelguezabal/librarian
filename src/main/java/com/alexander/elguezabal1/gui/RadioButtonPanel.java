/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.gui;

import com.alexander.elguezabal1.Library;
import com.alexander.elguezabal1.gui.listeners.SearchBarListener;
import com.alexander.elguezabal1.gui.listeners.SearchButtonListeners;
import com.alexander.elguezabal1.library.SearchType;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author Alex
 */
public class RadioButtonPanel extends JPanel implements ItemListener {
    
    public Frame frame;
    
    public JRadioButton genres;
    public JRadioButton authors;
    public JRadioButton languages;
    public JRadioButton search;
    public JRadioButton all;
    
    private ButtonGroup buttonGroup;
    
    private JTextField jTextArea;
    
    public final String TEXT_AREA_PREFILL = "Enter your book search here";
    
    private JPanel listOfButtons;
    private JScrollPane scrollPane;
    private List<JRadioButton> radioButtonsForTypes;
    private ButtonGroup radioButtonForTypesGroup;
   
    private JPanel searchArea;
    public JTextField searchBox;
    private JButton searchButton;
    

    /**
     * Constructor for RadioButton table
     * 
     * @param frame instance frame for this panel
     */
    public RadioButtonPanel(Frame frame) {
        // Sets the instance of the frame
        this.frame = frame;
        
        //this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.setLayout(new FlowLayout());
        this.setSize(1000,500);

        //((BorderLayout) this.getLayout()).setVgap(10);
        
        init();
        
        setMaximumSize(getPreferredSize());
        
        // Sets up the list of button bannel
        this.listOfButtons = new JPanel();
        this.listOfButtons.setLayout(new BoxLayout(listOfButtons, BoxLayout.PAGE_AXIS));
        
        this.scrollPane = new JScrollPane();
        this.scrollPane.setViewportView(listOfButtons);
        this.scrollPane.setBounds(0, 0, 1000, 200);
        this.scrollPane.setVerticalScrollBar(new JScrollBar());
        this.scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        frame.add(this.scrollPane, BorderLayout.EAST);
        
        
        // Does the list of button(s) group
        this.radioButtonForTypesGroup = new ButtonGroup();
        
        
        // Does the search text field
        this.searchArea = new JPanel();
        this.searchArea.setLayout(new BoxLayout(searchArea, BoxLayout.LINE_AXIS));
        
        // Does the search box
        this.searchBox = new JTextField();
        this.searchBox.setText(TEXT_AREA_PREFILL);
        this.searchBox.setForeground(Color.LIGHT_GRAY);
        this.searchBox.setMaximumSize(new Dimension(500,50));
        
        // Does the search button
        this.searchButton = new JButton("<html> Click to Search<br>The Library </html>");
        this.searchButton.setMaximumSize(new Dimension(200,50));

        // Adding search bar listeners
        SearchBarListener listenerInstance = new SearchBarListener(this);
        this.searchBox.addMouseListener(listenerInstance);
        this.searchButton.addActionListener(listenerInstance);

        this.searchArea.add(this.searchBox);
        this.searchArea.add(this.searchButton);
    }
    
    /**
     * Initilizes all the components
     */
    private void init() {
                
        this.genres = new JRadioButton("Genres");
        this.authors = new JRadioButton("Authors");
        this.languages = new JRadioButton("Languages");
        this.search = new JRadioButton("Search");
        this.all = new JRadioButton("All");
        
        this.getGenres().addItemListener(this);
        this.getAuthors().addItemListener(this);
        this.getLanguages().addItemListener(this);
        this.getSearch().addItemListener(this);
        this.getAll().addItemListener(this);
        
        this.buttonGroup = new ButtonGroup();
        
        this.buttonGroup.add(getGenres());
        this.buttonGroup.add(getAuthors());
        this.buttonGroup.add(getLanguages());
        this.buttonGroup.add(getSearch());
        this.buttonGroup.add(getAll());

        //add(jTextArea);
        add(new JLabel("Click one of these Buttons to Sort the Library: "));
        add(getGenres());
        add(getAuthors());
        add(getLanguages());
        add(getSearch());
        add(getAll());
    }
    
    /**
     * Adds the list of buttons or a search bar based off of
     * {@code buttonGroup}'s selection.
     * 
     * @param value Text inside the button to be parsed
     */
    private void listOfButtons(String value) {       
        SearchType searchType = SearchType.get(value);
        
        
        // If incorrect button value was passed through, stop.
        if(searchType == null) return;
        
        // Removes the search area always, it can/will be readded if search is clicked
        getFrame().remove(searchArea);
        // Removes all listed radio buttons                
        if(radioButtonsForTypes != null) {
            // Loops through all radio buttons that exist and removes them.
            for(JRadioButton n : radioButtonsForTypes) {            
                //Button group
                radioButtonForTypesGroup.remove(n);
                
                Container parent = n.getParent();
                
                // Don't want to remove nothing
                if(parent == null) continue;
                
                // Deletes n from parent and updates the panel.
                parent.remove(n);
                parent.validate();
                parent.repaint();
               
                getFrame().validate();
                getFrame().repaint();
            }
                      
        }
        
        // Adds a text box for searching books and a button
        switch (searchType) {
            case SEARCH -> {
                doSearch();
                return;
            }
            case ALL -> {
                doAll();
                return;
            }
            default -> {
                if(searchArea.isVisible())
                    getFrame().remove(searchArea);
                validate();
                getFrame().validate();
                getFrame().repaint();
            }
        }
        
        // ReInstantiates all existing radio buttons
        this.radioButtonsForTypes = new ArrayList<>();
    
        SearchButtonListeners instance = new SearchButtonListeners(this);
        
        // Used types
        Set<String> used = new HashSet<>();
        for(String n : Library.libraryManager.getAllOfASearchType(searchType)) {
            if(used.contains(n)) continue;
            used.add(n);
            
            // Creates the button
            JRadioButton button = new JRadioButton(n);
            button.addItemListener(instance);
                       
            // Adds the button in
            listOfButtons.add(button);
            radioButtonsForTypes.add(button);
            radioButtonForTypesGroup.add(button);
        }
        
        //Add buttons here
        //frame.add(listOfButtons, BorderLayout.EAST);
        validate();
        getFrame().validate();
    }
    
    /**
     * Activates the search panel
     */
    private void doSearch() {
        
        if(searchArea == null) return;
        
        // ReFormats the text inside a search box
        this.getSearchBox().setText(TEXT_AREA_PREFILL);
        this.getSearchBox().setForeground(Color.LIGHT_GRAY);
        
        // Adds the search panel to the middle of the frame
        getFrame().add(searchArea, BorderLayout.CENTER);
        validate();
        getFrame().validate();
        getFrame().repaint();
    }

     /**
     * Activates the search panel
     */
    private void doAll() {
        Library.frame.libraryTable.displayAll();
    }
    
    /**
     * @return the TEXT_AREA_PREFILL
     */
    public String getTEXT_AREA_PREFILL() {
        return TEXT_AREA_PREFILL;
    }
    
    /**
     * Action listener for the main buttons
     * @param evt 
     */
    @Override
    public void itemStateChanged(ItemEvent evt) {
        if(evt.getSource() instanceof JRadioButton) {
            
            JRadioButton jRadioButton = (JRadioButton) evt.getSource();
            
            if(jRadioButton.isSelected()) {
                // If the Button is from the header
                if(SearchType.get(jRadioButton.getText()) != null) {
                    listOfButtons(jRadioButton.getText());              
                } 
                
                // If the button is from a list
                else {
                    SearchType searchType = null;
                }
            }
        }        
    }

    /**
     * @return the frame
     */
    public Frame getFrame() {
        return frame;
    }

    /**
     * @return the searchBox
     */
    public JTextField getSearchBox() {
        return searchBox;
    }

    /**
     * @return the genres
     */
    public JRadioButton getGenres() {
        return genres;
    }

    /**
     * @return the authors
     */
    public JRadioButton getAuthors() {
        return authors;
    }

    /**
     * @return the languages
     */
    public JRadioButton getLanguages() {
        return languages;
    }

    /**
     * @return the search
     */
    public JRadioButton getSearch() {
        return search;
    }

    /**
     * @return the all
     */
    public JRadioButton getAll() {
        return all;
    }
    
}
