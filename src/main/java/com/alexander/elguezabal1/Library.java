/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1;

import com.alexander.elguezabal1.library.LibraryManager;
import com.alexander.elguezabal1.gui.Frame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Alex
 */
public class Library {
    
    // Instance variables
    public static LibraryManager libraryManager;
    public static Frame frame;
    
    public static void main(String[] args) {
        
        // Assigns the library manager
        libraryManager = new LibraryManager();
        
        // Gets and cahces the library from the file
        getLibraryManager().cacheLibrary("library.txt");
        
        // Prints books
        getLibraryManager().printBooks();
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Assigns the frame
                frame = new Frame();
            }
        });
    }

    /**
     * @return the libraryManager
     */
    public static LibraryManager getLibraryManager() {
        return libraryManager;
    }

    /**
     * @return the frame
     */
    public static Frame getFrame() {
        return frame;
    }
    
}
