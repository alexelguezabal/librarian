/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.library;

import com.alexander.elguezabal1.Library;

/**
 *
 * @author Alex
 */
public enum SearchType {
 
    ALL("All"),
    SEARCH("Search"),
    AUTHOR("Authors"),
    LANGUAGE("Languages"),
    GENRES("Genres");

    private String formatted;
    
    SearchType(String formatted) {
        this.formatted = formatted;
    }
    
    public String getFormatted() {
        return formatted;
    }
    
    public static SearchType get(String value) {
        for(SearchType n : SearchType.values()) {
            if(n.getFormatted().equalsIgnoreCase(value))
                return n;
        }
        return null;
    }
    
    public static SearchType getFromButtons() {
        if(Library.getFrame().radioButtonPanel.getAuthors().isSelected()) {
            return SearchType.AUTHOR;
        } else if(Library.getFrame().radioButtonPanel.getGenres().isSelected()) {
            return SearchType.GENRES;
        } else if(Library.getFrame().radioButtonPanel.getLanguages().isSelected()) {
            return SearchType.LANGUAGE;
        } else if(Library.getFrame().radioButtonPanel.getSearch().isSelected()) {
            return SearchType.SEARCH;
        } else if(Library.getFrame().radioButtonPanel.getAll().isSelected()) {
            return SearchType.ALL;
        }
        
        return null;
    }
}
