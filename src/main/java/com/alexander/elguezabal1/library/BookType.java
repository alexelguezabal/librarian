package com.alexander.elguezabal1.library;

import com.alexander.elguezabal1.library.shelf.Cookbook;
import com.alexander.elguezabal1.library.shelf.Dictionary;
import com.alexander.elguezabal1.library.shelf.GraphicNovel;
import com.alexander.elguezabal1.library.shelf.Novel;
import java.util.Arrays;

/**
 * Stores each Book and their associated type
 * @author Alex
 */
public enum BookType {
    
    DICTIONARY(1, Dictionary.class),
    COOKBOOK(2, Cookbook.class),
    NOVEL(3, Novel.class),
    GRAPHIC_NOVEL(4, GraphicNovel.class);

    private int value;
    public Class<? extends Book> bookClass;
    
    /**
     * Constructor
     * @param value value for each book 
     * @param bookClass class representing the book type
     */
    BookType(int value, Class<? extends Book> bookClass) {
        this.value = value;
        this.bookClass = bookClass;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }
    
    /**
     * @return the bookClass
     */
    public Class<? extends Book> getBookClass() {
        return bookClass;
    }
    
    /**
     * Finds a book type based off a 
     * @param i
     * @return 
     */
    public static BookType get(int i) {
        return Arrays.stream(BookType.values())
                .filter(n -> n.getValue() == i)
                .findFirst()
                .get();
    }

}
