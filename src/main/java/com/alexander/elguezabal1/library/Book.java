package com.alexander.elguezabal1.library;

import com.alexander.elguezabal1.library.interfaces.CompareableBook;

/**
 * Generic class for a book
 * @author Alex
 */
public abstract class Book implements Comparable<CompareableBook> {

    private String title;
    private String publisher;
    private int pageCount;
    
    /**
     * Constructor for a Book
     * 
     * @param title Title of this Book
     * @param publisher Name of this book's publisher
     * @param pageCount amount of pages in this book
     */
    public Book(String title, String publisher, int pageCount) {
        this.title = title;
        this.publisher = publisher;
        this.pageCount = pageCount;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * @param publisher the publisher to set
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * @return the pageCount
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * @param pageCount the pageCount to set
     */
    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }    
    
    /**
     * To String method for a book
     * @return the title pagecount and publisher of a book in preFormat
     */
    @Override
    public String toString() {
        return String.format("%s,%s,%s,", title, ""+pageCount, publisher);
    }
    
    /**
     * Compare to method for all books
     * Note. All Books must be a NonFiction or Fiction books
     * 
     * @param o That books to be compared to this book instance
     * @return 1 if this is bigger, 0 if equal, and -1 if that one is bigger
     */
    @Override
    public int compareTo(CompareableBook o) {
        
        // Initilizing both variables
        char thisLetter = Character.toLowerCase(((CompareableBook) this).getCompareString().charAt(1));
        char thatLetter = Character.toLowerCase(o.getCompareString().charAt(1));
                
        // Returns the value of this compare to
        // 1 if this is bigger, 0 if equal, and -1 if that one is bigger
        return thisLetter == thatLetter ? 0 : (thisLetter > thatLetter) ? 1 : -1;
    }
    
}
