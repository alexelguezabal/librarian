package com.alexander.elguezabal1.library;

import com.alexander.elguezabal1.library.interfaces.CompareableBook;
import com.alexander.elguezabal1.library.interfaces.Findable;

/**
 * Abstract class for a NonFiction book
 * @author Alex
 */
public abstract class NonFictionBook extends Book implements CompareableBook, Findable {

    private String language;
    
    /**
     * Constructor for a NonFiction book
     * 
     * @param title Title of this Book
     * @param publisher Name of this book's publisher
     * @param pageCount amount of pages in this book
     * @param language The language that this book is written in
     */
    public NonFictionBook(String title, String publisher, int pageCount, String language) {
        super(title, publisher, pageCount);
        
        this.language = language;
    }
    
    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }  
    
    /**
     * To String method.
     * @return 
     */
    @Override
    public String toString() {       
        return super.toString() + String.format("%s,,", language);
    }
    
    /**
     * Compare Getter
     * @return 
     */
    @Override
    public String getCompareString() {
        return this.getLanguage();
    }
    
    /**
     * Finds all the Searchable Strings
     * For this instance
     * 
     * @param searchType The type to search
     * @return The value from this instance
     */
    @Override
    public String getSearchValue(SearchType searchType) {
        String ret = "";
        
        switch(searchType) {
            case AUTHOR: break;
            
            case LANGUAGE: {
                ret += getLanguage();                
            } break;

            case GENRES: break;    
            
            case SEARCH: break;
            
            case ALL: break;
        }
        
        return ret;
    }
    
     /**
     * Determines if this instance contains a certain Search type attribute
     * Ex. If a Cookbook (Findable) contains a certian language that has been selected
     * 
     * @param searchType Search Type
     * @param value Value to be found
     * @return true if this Findable has the contained {@code value} in the {@code searchType} attribute
     */
    @Override
    public boolean has(SearchType searchType, String value) {
        switch(searchType) {
            case AUTHOR: break;
            
            case LANGUAGE: {
                return getLanguage().equalsIgnoreCase(value);
            }

            case GENRES: break;    
            
            case SEARCH: break;
            
            case ALL: break;
        }
        
        return false;
    }

}
