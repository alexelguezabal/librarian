package com.alexander.elguezabal1.library.shelf;

import com.alexander.elguezabal1.library.NonFictionBook;
import com.alexander.elguezabal1.library.interfaces.Searchable;

/**
 * A NonFiction Book used for Cooking
 * @author Alex 
 */
public class Cookbook extends NonFictionBook implements Searchable {
 
    private String topic;

    /**
     * Default Constructor
     */
    public Cookbook() {
        super("", "", 0, "");
        this.topic = "";
    }
    
    /**
     * Constructor for a Cookbook
     * 
     * @param title Title of this Book
     * @param publisher Name of this book's publisher
     * @param pageCount amount of pages in this book
     * @param language The language that this book is written in
     * @param topic The topic of this cookbook
     */
    public Cookbook(String title, String publisher, int pageCount, String language, String topic) {
        super(title, publisher, pageCount, language);
        
        this.topic = topic;
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }
    
     /**
     * To String method.
     * @return 
     */
    @Override
    public String toString() {       
        return super.toString() + String.format("%s,,", topic);
    }

    /**
     * Determines if this book should be added by search
     * Checks to see if any attributes of this class contain {@code value}
     * 
     * @param value Substring value
     * @return a boolean value determining if this book should be added.
     */
    @Override
    public boolean contains(String value) {
        return this.getTitle().toLowerCase().contains(value) || 
                this.getPublisher().toLowerCase().contains(value) || 
                this.getLanguage().toLowerCase().contains(value) || 
                this.getTopic().toLowerCase().contains(value);
    }

    
}
