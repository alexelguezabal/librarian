/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.library.shelf;

import com.alexander.elguezabal1.library.FictionBook;
import com.alexander.elguezabal1.library.interfaces.Searchable;

/**
 * A Fiction Book that shows a book with pictures, a cartoon
 * @author Alex
 */
public class GraphicNovel extends FictionBook implements Searchable {
   
    public String illustrator;

    /**
     * Default Constructor
     */
    public GraphicNovel() {
        super("", "", 0, "", "");
        this.illustrator = "";
    }
    
     /**
     * Constructor for a Novel
     * 
     * @param title Title of this Book
     * @param publisher Name of this book's publisher
     * @param pageCount amount of pages in this book
     * @param author Name of this book's author
     * @param genre the genre of this book
     * @param illustrator Name of the illustrator of this book
     */
    public GraphicNovel(String title, String publisher, int pageCount, String author, String genre, String illustrator) {
        super(title, publisher, pageCount, author, genre);
        
        this.illustrator = illustrator;
    }

    /**
     * @return the illustrator
     */
    public String getIllustrator() {
        return illustrator;
    }

    /**
     * @param illustrator the illustrator to set
     */
    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }
    
    /**
     * To String method.
     * @return 
     */
    @Override
    public String toString() {       
        return super.toString() + String.format("%s,,", illustrator);
    }    
    
     /**
     * Determines if this book should be added by search
     * Checks to see if any attributes of this class contain {@code value}
     * 
     * @param value Substring value
     * @return a boolean value determining if this book should be added.
     */
    @Override
    public boolean contains(String value) {
        return this.getTitle().toLowerCase().contains(value) || 
                this.getPublisher().toLowerCase().contains(value) || 
                this.getAuthor().toLowerCase().contains(value) || 
                this.getGenre().toLowerCase().contains(value) ||
                this.getIllustrator().toLowerCase().contains(value);
    }
}
