package com.alexander.elguezabal1.library.shelf;

import com.alexander.elguezabal1.library.NonFictionBook;
import com.alexander.elguezabal1.library.interfaces.Searchable;

/**
 * A NonFiction Book used for storing words
 * @author Alex
 */
public class Dictionary extends NonFictionBook implements Searchable {
    
    private int versionNumber;

    /**
     * Default Constructor
     */
    public Dictionary() {
        super("", "", 0, "");
        this.versionNumber = 0;
    }
    
    /**
     * Constructor for a Dictionary book
     * 
     * @param title Title of this Book
     * @param publisher Name of this book's publisher
     * @param pageCount amount of pages in this book
     * @param language The language that this book is written in
     * @param versionNumber The version number of this book
     */
    public Dictionary(String title, String publisher, int pageCount, String language, int versionNumber) {
        super(title, publisher, pageCount, language);
        
        this.versionNumber = versionNumber;
    }

    /**
     * @return the versionNumber
     */
    public int getVersionNumber() {
        return versionNumber;
    }

    /**
     * @param versionNumber the versionNumber to set
     */
    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }
    
     /**
     * To String method.
     * @return 
     */
    @Override
    public String toString() {       
        return super.toString() + String.format("%s,,", ""+versionNumber);
    }
    
     /**
     * Determines if this book should be added by search
     * Checks to see if any attributes of this class contain {@code value}
     * 
     * @param value Substring value
     * @return a boolean value determining if this book should be added.
     */
    @Override
    public boolean contains(String value) {
        return this.getTitle().toLowerCase().contains(value) || 
                this.getPublisher().toLowerCase().contains(value) || 
                this.getLanguage().toLowerCase().contains(value);
    }
}
