/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.library.shelf;

import com.alexander.elguezabal1.library.FictionBook;
import com.alexander.elguezabal1.library.SearchType;
import com.alexander.elguezabal1.library.interfaces.Searchable;

/**
 * A Fiction Book used for storing stories
 * @author Alex
 */
public class Novel extends FictionBook implements Searchable {
   
    private boolean isPartOfASeries;

    /**
     * Default Constructor
     */
    public Novel() {
        super("", "", 0, "", "");
        this.isPartOfASeries = false;
    }
    
     /**
     * Constructor for a Novel
     * 
     * @param title Title of this Book
     * @param publisher Name of this book's publisher
     * @param pageCount amount of pages in this book
     * @param author Name of this book's author
     * @param genre the genre of this book
     * @param isPartOfASeries Determines if this book is part of a series
     */
    public Novel(String title, String publisher, int pageCount, String author, String genre, boolean isPartOfASeries) {
        super(title, publisher, pageCount, author, genre);
        
        this.isPartOfASeries = isPartOfASeries;
    }

    /**
     * @return the isPartOfASeries
     */
    public boolean isIsPartOfASeries() {
        return isPartOfASeries;
    }

    /**
     * @param isPartOfASeries the isPartOfASeries to set
     */
    public void setIsPartOfASeries(boolean isPartOfASeries) {
        this.isPartOfASeries = isPartOfASeries;
    }

     /**
     * To String method.
     * @return 
     */
    @Override
    public String toString() {       
        return super.toString() + String.format("%s,,", isPartOfASeries);
    }
    
    /**
     * Determines if this book should be added by search
     * Checks to see if any attributes of this class contain {@code value}
     * 
     * @param value Substring value
     * @return a boolean value determining if this book should be added.
     */
    @Override
    public boolean contains(String value) {
        return this.getTitle().toLowerCase().contains(value) || 
                this.getPublisher().toLowerCase().contains(value) || 
                this.getAuthor().toLowerCase().contains(value) || 
                this.getGenre().toLowerCase().contains(value);
    }

}
