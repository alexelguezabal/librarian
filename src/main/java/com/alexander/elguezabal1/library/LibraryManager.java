/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.alexander.elguezabal1.library;

import com.alexander.elguezabal1.library.interfaces.CompareableBook;
import com.alexander.elguezabal1.library.shelf.Cookbook;
import com.alexander.elguezabal1.library.shelf.Dictionary;
import com.alexander.elguezabal1.library.shelf.GraphicNovel;
import com.alexander.elguezabal1.library.shelf.Novel;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import com.alexander.elguezabal1.library.interfaces.Findable;
import com.alexander.elguezabal1.library.interfaces.Searchable;

/**
 * Manager Class for the Library
 * Deals With
 *  * Book Reading
 *  * Book Writing
 *  * Book Cache
 * @author Alex
 */
public class LibraryManager {
    
    public LibraryManager() {
        library = new HashMap<>();
    }
    
    // Title to the book refrence
    public HashMap<String, Book> library;
    
    /**
     * Loads a file consisting of books into the library
     * 
     * @param path path located in this project where the book database is stored
     */    
    public void cacheLibrary(String path) {
        
        // Rese the current library
        library = new HashMap<>();
        
        try {
            // Creates the file reading objects
            File file = getFile(path);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            
            int numberOfBooks = Integer.valueOf(bufferedReader.readLine());
            
            // Reads the file and appends values to the library
            while(bufferedReader.ready()) {
                
                String[] values = bufferedReader.readLine().split(",");
                
                int value = Integer.valueOf(values[0]);
                String title = values[1];
                
                System.out.println(value);
                
                // Type of book
                BookType bookType = BookType.get(value);
                
                // Iterates over the types of books and creates corresponding objects
                switch(bookType) {
                    case COOKBOOK: {
                        Cookbook cookbook = new Cookbook(title, values[2], Integer.valueOf(values[3].trim()), values[4], values[5]);
                        getLibrary().put(title, cookbook);
                    }break;
                    case DICTIONARY: {
                        Dictionary dictionary = new Dictionary(title, values[2], Integer.valueOf(values[3].trim()), values[4], Integer.valueOf(values[5].trim()));
                        getLibrary().put(title, dictionary);
                    }break;
                    case NOVEL: {
                        Novel novel = new Novel(title, values[2], Integer.valueOf(values[3].trim()), values[4], values[5], getBooleanValue(values[6]));
                        getLibrary().put(title, novel);
                    }break;
                    case GRAPHIC_NOVEL: {
                        GraphicNovel graphicNovel = new GraphicNovel(title, values[2], Integer.valueOf(values[3].trim()), values[4], values[5], values[6]);
                        getLibrary().put(title, graphicNovel);
                    }break;
                }                
            }
            
        } catch(IOException e) {
            e.printStackTrace();
        } catch(IndexOutOfBoundsException e) {
            System.out.println("Error parsing files, please refer to code.");
            e.printStackTrace();
        }
    }
    
    /**
     * Prints out all the books
     */
    public void printBooks() {
        getLibrary().values().forEach(n -> System.out.println(n));
    }
    
    /**
     * Gets all the books information
     * @return a List of each books toString method.
     */
    public List<String> getBooksInformation() {
        return getLibrary().values().stream()
                // Sorts based Author And Language
                .sorted((x, y) -> x.compareTo((CompareableBook) y))
                .map(n -> formatStringForJLabel(n.toString()))
                .collect(Collectors.toList());   
    }
    
     /**
     * @return the library
     */
    public HashMap<String, Book> getLibrary() {
        return library;
    }
    
    /**
     * Finds all the books types for a certian search type
     * @param seachType The type to look for
     * @return A List of the types from a specific search type
     */
    public List<String> getAllOfASearchType(SearchType seachType) {
        
        ArrayList<String> list = new ArrayList<>();
        
        // Loops over every search type to find all the books and adds them to list
        for(Book n : library.values()) {
            String toAdd = ((Findable)n).getSearchValue(seachType);
            if(!toAdd.isBlank()) list.add(toAdd);
        }
        
        return list;
    }
    
    /**
     * Searches over all books that contain the attribute value
     * 
     * @param value Value to be searched over
     * @return a list of applicable books
     */
    public List<String> findSearchedBooks(String value) {
        List<String> list = new ArrayList<>();

        // Loops over all Searchables to determine if this book contains {@code value}
        for(Book n : library.values().stream().sorted((x, y) -> x.compareTo((CompareableBook) y)).collect(Collectors.toList())) {
            // If the book is applicable, then it is formatted and added to list
            if(((Searchable)n).contains(value.toLowerCase())) {
                list.add(formatStringForJLabel(n.toString()));
            }
        }
        
        // Output
        return list;
    }
    
    /**
     * Finds all the books with {@code value}
     * 
     * @param searchType Search type to look for
     * @param value value to look for
     * @return List of formatted books with {@value}
     */
    public List<String> findFoundBooks(SearchType searchType, String value) {
        List<String> list = new ArrayList<>();
        
        // Loops over all Findables to determine if this book contains {@code value}
        for(Book n : library.values().stream().sorted((x, y) -> x.compareTo((CompareableBook) y)).collect(Collectors.toList())) {
            // If the book is applicable, then it is formatted and added to list
            if(((Findable)n).has(searchType, value)) {
                list.add(formatStringForJLabel(n.toString()));
            }
        }
        
        // Output
        return list;
    }
    
    /**
     * Private methods
     */
    
    /**
     * Gets and returns a file
     * 
     * @param path Path of the file
     * @return file to be returned
     * @throws FileNotFoundException thrown if the file does not exist or path is incorrect.
     */
    private File getFile(String path) throws FileNotFoundException {
        
        File file = new File(path);
        
        if(file == null || !file.exists())
            throw new FileNotFoundException();
        
        return file;
    }
    
    /**
     * Gets a boolean value for a (Y/N) input 
     * @param n a yes or no input
     * @return the boolean value of {@code n}
     */
    private boolean getBooleanValue(String n) {
        return n.contains("Y") || n.contains("y");
    }
    
       /**
     * Fixes a string to be formatted for a box
     * 
     * @param string to be formatted
     * @return a formatted string
     */
    private String formatStringForJLabel(final String string) {
        
        String output = "";
        
        // Split String length
        String[] split = string.split(",");
        
        // Does title
        final int titleLength = 40;
        for(int i = 0; i < (titleLength - split[0].length()); i++) {
            output += " ";
        }
        output += split[0] + " | ";
                
        //Does ID
        final int idLength = 7;
        for(int i = 0; i < (idLength - split[1].length()); i++) {
            output += " ";
        }
        output += split[1] + " | ";
        
        //Does Publisher
        final int publisherLength = 20;
        for(int i = 0; i < (publisherLength - split[2].length()); i++) {
            output += " ";
        }
        output += split[2] + " | ";
        
        //Does Language/Author
        final int languageAuthorLength = 25;
        for(int i = 0; i < (languageAuthorLength - split[3].length()); i++) {
            output += " ";
        }
        output += split[3] + " | ";
        
        
        //Does VersionNumber/Genre/Topic
        final int versionNumberGenreTopic = 25;
        for(int i = 0; i < (versionNumberGenreTopic - split[4].length()); i++) {
            output += " ";
        }
        output += split[4] + " | "; 
        
        //Does Series/Illustrator
        final int seriesIllustrator = 25;
        for(int i = 0; i < (seriesIllustrator - split[5].length()); i++) {
            output += " ";
        }
        output += split[5] + " | "; 
        
        // Returns the formatted output string.
        return output;
    }
     
}
