/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.library.interfaces;

/**
 *
 * @author Alex
 */
public interface Searchable {
    
    /**
     * Determines if this book contains parts of a substring in any catigory
     * 
     * @param value Value to be searched
     * @return boolean if this book should be added or not.
     */
    public abstract boolean contains(String value);
    
}
