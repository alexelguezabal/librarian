/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.library.interfaces;

import com.alexander.elguezabal1.library.SearchType;

/**
 *
 * @author Alex
 */
public interface Findable {
    
    /**
     * Gets the value for a specific type
     */
    public abstract String getSearchValue(SearchType searchType);
    
    /**
     * Determines if this instance contains a certain Search type attribute
     * Ex. If a Cookbook (Findable) contains a certian language that has been selected
     * 
     * @param searchType Search Type
     * @param value Value to be found
     * @return true if this Findable has the contained {@code value} in the {@code searchType} attribute
     */
    public abstract boolean has(SearchType searchType, String value);
}
