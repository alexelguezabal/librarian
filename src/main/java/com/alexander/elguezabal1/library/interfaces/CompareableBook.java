/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.alexander.elguezabal1.library.interfaces;

/**
 *
 * @author Alex
 */
public interface CompareableBook {

    /**
     * Gets the String to compare to another compareable
     * @return 
     */
    public abstract String getCompareString();
    
    
}
