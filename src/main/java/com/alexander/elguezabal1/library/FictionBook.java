package com.alexander.elguezabal1.library;

import com.alexander.elguezabal1.library.interfaces.CompareableBook;
import com.alexander.elguezabal1.library.interfaces.Findable;

/**
 * Generic class for a Fiction book
 * @author Alex
 */
public abstract class FictionBook extends Book implements CompareableBook, Findable {

    private String author;
    private String genre;
    
    /**
     * Constructor for a FictionBook
     * 
     * @param title Title of this Book
     * @param publisher Name of this book's publisher
     * @param pageCount amount of pages in this book
     * @param author Name of this book's author
     * @param genre the genre of this book
     */
    public FictionBook(String title, String publisher, int pageCount, String author, String genre) {
        super(title, publisher, pageCount);
        
        this.author = author;
        this.genre = genre;
    }
    
    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }
    
    /**
     * To String method.
     * @return 
     */
    @Override
    public String toString() {       
        return super.toString() + String.format("%s,%s,", author, genre);
    }

     /**
     * Compare Getter
     * @return 
     */
    @Override
    public String getCompareString() {
        return this.getAuthor();
    }
    
        /**
     * Finds all the Searchable Strings
     * For this instance
     * 
     * @param searchType The type to search
     * @return The value from this instance
     */
    @Override
    public String getSearchValue(SearchType searchType) {
        String ret = "";
        
        switch(searchType) {
            case AUTHOR: {
                ret += getAuthor();
            };break;
            
            case LANGUAGE: break;

            case GENRES: {
                ret += getGenre();
            };    
            
            case SEARCH: break;
            
            case ALL: break;
        }
        
        return ret;
    }
    
    /**
     * Determines if this instance contains a certain Search type attribute
     * Ex. If a Cookbook (Findable) contains a certian language that has been selected
     * 
     * @param searchType Search Type
     * @param value Value to be found
     * @return true if this Findable has the contained {@code value} in the {@code searchType} attribute
     */
    @Override
    public boolean has(SearchType searchType, String value) {
        switch(searchType) {
            case AUTHOR: return getAuthor().equalsIgnoreCase(value);
            
            case LANGUAGE: break;

            case GENRES: return getGenre().equalsIgnoreCase(value);    
            
            case SEARCH: break;
            
            case ALL: break;
        }
        
        return false;
    }
    
}
